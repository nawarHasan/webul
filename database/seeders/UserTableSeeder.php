<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $user = User::create([
            'name' => 'user',
            'email'=>'nawar@gggmail',
            'password' => bcrypt('0945458778'), // optional
            'country' => 'Syria', // optional
        ]);

    $user->addRole('admin');
}
}
