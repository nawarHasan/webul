<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\AccessTokenController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::apiResource('user',\App\Http\Controllers\Api\UserController::class);
Route::apiResource('team',\App\Http\Controllers\Api\TeamController::class);
Route::apiResource('magazine',\App\Http\Controllers\Api\MagazineController::class);
Route::post('auth/token/user', [AccessTokenController::class,'storeUser']);
Route::post('register/user', [\App\Http\Controllers\Api\UserController::class,'storeUser']);
Route::apiResource('faq',\App\Http\Controllers\Api\FaqController::class);
Route::apiResource('advertisement',\App\Http\Controllers\Api\AdvertisementController::class);
Route::apiResource('performance',\App\Http\Controllers\Api\PerformanceController::class);
Route::apiResource('live',\App\Http\Controllers\Api\LiveController::class);
Route::apiResource('notification',\App\Http\Controllers\Api\NotificationController::class);
Route::apiResource('feature',\App\Http\Controllers\Api\FeatureController::class);
Route::apiResource('followuppages',\App\Http\Controllers\Api\FollowUpPagesController::class);
Route::apiResource('recentachievement',\App\Http\Controllers\Api\RecentAchievementController::class);
Route::apiResource('trainningvideo',\App\Http\Controllers\Api\TrainningVideoController::class);
Route::apiResource('topnotification',\App\Http\Controllers\Api\TopNotificationController::class);
Route::apiResource('livefacebook',\App\Http\Controllers\Api\LiveFaceBookController::class);
Route::apiResource('livetwitter',\App\Http\Controllers\Api\LiveTwitterController::class);
Route::apiResource('liveyoutube',\App\Http\Controllers\Api\LiveYoutubeController::class);
Route::apiResource('liveinstagram',\App\Http\Controllers\Api\LiveInstagramController::class);
Route::apiResource('livelinkedin',\App\Http\Controllers\Api\LivelinkedinController::class);
Route::apiResource('subscription',\App\Http\Controllers\Api\SubscriptionController::class);

// Route::post('register', 'RegistrationController@register');
// Route::get('email/verify/{id}', 'VerificationController@verify')->name('verification.verify');
// Route::get('email/resend', \App\Http\Controllers\Api\VerificationController,'resend')->name('verification.resend');
 Route::post('password/email', [\App\Http\Controllers\Api\ForgotPasswordController::class,'forgot']);
 Route::post('password/reset', [\App\Http\Controllers\Api\ForgotPasswordController::class,'reset']);

