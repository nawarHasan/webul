<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Advertisement;
use Illuminate\Http\Request;
use App\Http\Requests\PostStoreRequest;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class AdvertisementController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(){

        $advertisements = Advertisement::all();
        return  response([
           'advertisements'=>$advertisements
       ],200);
       }

       public function store(Request $request){


            $data['name'] = $request->name;
            $data['description'] = $request->description;

    if ($request->hasFile('image')) {
        $file_ext=$request->image->getClientOriginalExtension() ;
        $file_name=time().'.'.$file_ext;
        $path='images';
        $request->image->move($path,$file_name);
        $data['image']=$file_name;
    }

    if ($request->hasFile('video')) {
        $file_ext=$request->video->getClientOriginalExtension() ;
        $file_name=time().'.'.$file_ext;
        $path='images';
        $request->video->move($path,$file_name);
        $data['video']=$file_name;
    }

    $advertisement= Advertisement::create($data);

            return response()->json([
                'status' => true,
                'message' => 'Advertisement Created Successfully',
                'advertisement' => $advertisement,
            ]);

}




       public function update(Request $request,$id){

           $advertisement= Advertisement::findOrFail($id);

           $data['name'] = $request->name;
           $data['description'] = $request->description;

           if ($request->hasFile('video')) {

               $oldvideo = $advertisement->video;
               $video = $request->file('image');
               $data['video'] = $this->videos($video, $oldvideo);
           }

           if ($request->hasFile('image')) {

            $oldimage = $advertisement->image;
            $image = $request->file('image');
            $data['image'] = $this->images($image, $oldimage);
        }
           $advertisement->update($request->all());
               return response()->json([
                   'status'=>true,
                   'data'=>$advertisement,
                   'message' => 'video Updated Successfully',
               ]);
       }

       public function destroy($id)
       {
           $advertisement = Advertisement::findOrFail($id);
               $advertisement->delete();
           return response()->json([
               'status'=>true,
               'message' => 'Request Information deleted Successfully',
           ]);
           }
       }
