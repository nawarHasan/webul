<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\LiveYoutube;
use Illuminate\Http\Request;

class LiveYoutubeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(){


        $liveyoutubes = LiveYoutube::all();
        return response([
           'liveyoutubes'=>$liveyoutubes
       ],200);
       }

       public function store(Request $request){

           $data['description'] = $request->description;
           $data['link'] = $request->link;

           $liveyoutube= LiveYoutube::create($data);

           return response()->json([
               'status' => true,
               'message' => 'liveyoutube Created Successfully',
               'liveyoutube' => $liveyoutube,
           ]);

       }


       public function update(Request $request,$id){

           $liveyoutube= LiveYoutube::findOrFail($id);
           $ $data['description'] = $request->description;
           $data['link'] = $request->link;

           $liveyoutube->update($request->all());
               return response()->json([
                   'status'=>true,
                   'data'=>$liveyoutube,
                   'message' => 'LiveYoutube Updated Successfully',
               ]);
       }

       public function destroy($id)
       {
           $liveyoutube = LiveYoutube::findOrFail($id);
               $liveyoutube->delete();
           return response()->json([
               'status'=>true,
               'message' => 'Request Information deleted Successfully',
           ]);
           }
        }

