<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\LiveLinkedin;
use Illuminate\Http\Request;

class LiveLinkedinController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(){


        $livelinkedins = LiveLinkedin::all();
        return response([
           'LiveLinkedins'=>$livelinkedins
       ],200);
       }

       public function store(Request $request){

           $data['description'] = $request->description;
           $data['link'] = $request->link;

           $livelinkedin= LiveLinkedin::create($data);

           return response()->json([
               'status' => true,
               'message' => 'LiveLinkedin Created Successfully',
               'livelinkedin' => $livelinkedin,
           ]);

       }


       public function update(Request $request,$id){

           $livelinkedin= LiveLinkedin::findOrFail($id);
           $data['description'] = $request->description;
           $data['link'] = $request->link;


           $livelinkedin->update($request->all());
               return response()->json([
                   'status'=>true,
                   'data'=>$livelinkedin,
                   'message' => 'livelinkedin Updated Successfully',
               ]);
       }

       public function destroy($id)
       {
           $livelinkedin = LiveLinkedin::findOrFail($id);
               $livelinkedin->delete();
           return response()->json([
               'status'=>true,
               'message' => 'Request Information deleted Successfully',
           ]);
           }
        }
