<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Subscription;
use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(){


        $subscriptions = Subscription::all();
        return  response([
           'subscriptions'=>$subscriptions
       ],200);
       }

       public function store(Request $request){

        $data['monthly_subscription'] = $request->monthly_subscription;
        $data['weekly_subscription'] = $request->weekly_subscription;

           $subscription= Subscription::create($data);

           return response()->json([
               'status' => true,
               'message' => 'Subscription Created Successfully',
               'subscription' => $subscription,
           ]);

       }


       public function update(Request $request,$id){

           $subscription= Subscription::findOrFail($id);
           $data['monthly_subscription'] = $request->monthly_subscription;
           $data['weekly_subscription'] = $request->weekly_subscription;


           $subscription->update($request->all());
               return response()->json([
                   'status'=>true,
                   'data'=>$subscription,
                   'message' => 'subscription Updated Successfully',
               ]);
       }

       public function destroy($id)
       {
           $subscription = Subscription::findOrFail($id);
               $subscription->delete();
           return response()->json([
               'status'=>true,
               'message' => 'Request Information deleted Successfully',
           ]);
           }
        }

