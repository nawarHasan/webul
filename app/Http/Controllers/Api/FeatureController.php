<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Feature;
use Illuminate\Http\Request;

class FeatureController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(){


        $features = Feature::all();
        return  response([
           'features'=>$features
       ],200);
       }

       public function store(Request $request){


        $data['name']  = $request->name;
        $data['description'] = $request->description;
        $data['icon'] = $request->icon;


           $feature= Feature::create($data);

           return response()->json([
               'status' => true,
               'message' => 'feature Created Successfully',
               'feature' => $feature,
           ]);

       }


       public function update(Request $request,$id){

           $feature= Feature::findOrFail($id);
           $data['name']  = $request->name;
           $data['description'] = $request->description;
           $data['icon'] = $request->icon;



           $feature->update($request->all());
               return response()->json([
                   'status'=>true,
                   'data'=>$feature,
                   'message' => 'feature Updated Successfully',
               ]);
       }

       public function destroy($id)
       {
           $feature = Feature::findOrFail($id);
               $feature->delete();
           return response()->json([
               'status'=>true,
               'message' => 'Request Information deleted Successfully',
           ]);
           }
       }
