<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Live;
use Illuminate\Http\Request;

class LiveController extends Controller
{
    public function index(){


        $lives = Live::all();
        return  response([
           'lives'=>$lives
       ],200);
       }

       public function store(Request $request){


           $data['answer']  = $request->answer;
           $data['question'] = $request->question;
           $data['link'] = $request->link;

           $live= Live::create($data);

           return response()->json([
               'status' => true,
               'message' => 'live Created Successfully',
               'live' => $live,
           ]);

       }


       public function update(Request $request,$id){

           $live= Live::findOrFail($id);
           $data['answer']  = $request->answer;
           $data['question'] = $request->question;


           $live->update($request->all());
               return response()->json([
                   'status'=>true,
                   'data'=>$live,
                   'message' => 'Live Updated Successfully',
               ]);
       }

       public function destroy($id)
       {
           $live = Live::findOrFail($id);
               $live->delete();
           return response()->json([
               'status'=>true,
               'message' => 'Request Information deleted Successfully',
           ]);
           }
        }
