<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\TopNotification;
use Illuminate\Http\Request;

class TopNotificationController extends Controller
{
    public function index(){


        $notofocations = TopNotification::all();
        return  response([
           'notofocations'=>$notofocations
       ],200);
       }

       public function store(Request $request){


           $data['name']  = $request->name;
           $data['description'] = $request->description;


           $TopNotification= TopNotification::create($data);

           return response()->json([
               'status' => true,
               'message' => 'TopNotification Created Successfully',
               'TopNotification' => $TopNotification,
           ]);

       }


       public function update(Request $request,$id){

           $TopNotification= TopNotification::findOrFail($id);
           $data['name']  = $request->name;
           $data['description'] = $request->question;;


           $TopNotification->update($request->all());
               return response()->json([
                   'status'=>true,
                   'data'=>$TopNotification,
                   'message' => 'TopNotification Updated Successfully',
               ]);
       }

       public function destroy($id)
       {
           $TopNotification = TopNotification::findOrFail($id);
               $TopNotification->delete();
           return response()->json([
               'status'=>true,
               'message' => 'Request Information deleted Successfully',
           ]);
           }
        }
