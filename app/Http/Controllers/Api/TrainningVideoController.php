<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\TrainningVideo;
use Illuminate\Http\Request;

class TrainningVideoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(){


        $trainning_videos = TrainningVideo::all();
        return  response([
           'trainning_videos'=>$trainning_videos
       ],200);
       }

       public function store(Request $request){

           $data['video_title'] = $request->video_title;
           $data['video_description'] = $request->video_description;
           $data['simple_description'] = $request->simple_description;

           if ($request->hasFile('video')) {
            $file_ext=$request->video->getClientOriginalExtension() ;
            $file_name=time().'.'.$file_ext;
            $path='images';
            $request->video->move($path,$file_name);
            $data['video']=$file_name;
        }

        if ($request->hasFile('image')) {
            $file_ext=$request->image->getClientOriginalExtension() ;
            $file_name=time().'.'.$file_ext;
            $path='images';
            $request->image->move($path,$file_name);
            $data['image']=$file_name;
        }
           $trainning_video= TrainningVideo::create($data);

           return response()->json([
               'status' => true,
               'message' => 'trainning_video Created Successfully',
               'trainning_video' => $trainning_video,
           ]);

       }


       public function update(Request $request,$id){

           $trainning_video= TrainningVideo::findOrFail($id);
           $data['video_title'] = $request->video_title;
           $data['video_description'] = $request->video_description;
           $data['simple_description'] = $request->simple_description;
           if ($request->hasFile('video')) {

            $oldvideo = $trainning_video->video;
            $video = $request->file('image');
            $data['video'] = $this->videos($video, $oldvideo);
        }
        if ($request->hasFile('image')) {

            $oldimage = $trainning_video->image;
            $image = $request->file('image');
            $data['image'] = $this->images($image, $oldimage);
        }
           $trainning_video->update($request->all());
               return response()->json([
                   'status'=>true,
                   'data'=>$trainning_video,
                   'message' => 'trainning_video Updated Successfully',
               ]);
       }

       public function destroy($id)
       {
           $trainning_video = TrainningVideo::findOrFail($id);
               $trainning_video->delete();
           return response()->json([
               'status'=>true,
               'message' => 'Request Information deleted Successfully',
           ]);
           }
       }

