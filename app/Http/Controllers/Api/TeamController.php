<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Team;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(){


         $teams = Team::all();
         return  response([
            'teams'=>$teams
        ],200);
        }

        public function store(Request $request){


            $data['name'] = $request->name;
            $data['namesection'] = $request->namesection;
            $data['description'] = $request->description;
            if ($request->hasFile('image')) {
                $file_ext=$request->image->getClientOriginalExtension() ;
                $file_name=time().'.'.$file_ext;
                $path='images';
                $request->image->move($path,$file_name);
                $data['image']=$file_name;
            }

            $team= Team::create($data);

            return response()->json([
                'status' => true,
                'message' => 'Team Created Successfully',
                'team' => $team,
            ]);

        }


        public function update(Request $request,$id){

            $team= Team::findOrFail($id);
            $data['name'] = $request->name;
            $data['namesection'] = $request->namesection;
            $data['description'] = $request->description;

            if ($request->hasFile('image')) {

                $oldimage = $team->image;
                $image = $request->file('image');
                $data['image'] = $this->images($image, $oldimage);
            }
            $team->update($request->all());
                return response()->json([
                    'status'=>true,
                    'data'=>$team,
                    'message' => 'team Updated Successfully',
                ]);
        }

        public function destroy($id)
        {
            $team = Team::findOrFail($id);
                $team->delete();
            return response()->json([
                'status'=>true,
                'message' => 'Request Information deleted Successfully',
            ]);
            }
        }

