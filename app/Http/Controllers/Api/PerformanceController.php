<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Performance;
use Illuminate\Http\Request;

class PerformanceController extends Controller
{
    /**
     * Display a listing of the resource.
     */

    public function index(){


         $performances = Performance::all();
         return  response([
            'performances'=>$performances
        ],200);
        }

        public function store(Request $request){


            $data['sympol']  = $request->sympol;
            $data['target'] = $request->target;
            $data['reached'] = $request->reached;
            $data['comment'] = $request->comment;
            $data['month'] = $request->month;

            $performance= Performance::create($data);

            return response()->json([
                'status' => true,
                'message' => 'performance Created Successfully',
                'performance' => $performance,
            ]);

        }


        public function update(Request $request,$id){

            $performance= Performance::findOrFail($id);
            $data['name']  = $request->name;
            $data['description'] = $request->description;
            $data['link'] = $request->link;
            $data['month'] = $request->month;

            $performance->update($request->all());
                return response()->json([
                    'status'=>true,
                    'data'=>$performance,
                    'message' => 'performance Updated Successfully',
                ]);
        }

        public function destroy($id)
        {
            $performance = Performance::findOrFail($id);
                $performance->delete();
            return response()->json([
                'status'=>true,
                'message' => 'Request Information deleted Successfully',
            ]);
            }
        }
