<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Notification;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     */
     public function index(){


        $notifications = Notification::all();
        return  response([
           'notifications'=>$notifications
       ],200);
       }

       public function store(Request $request){


        $data['name']  = $request->name;
        $data['number'] = $request->number;
        $data['description'] = $request->description;
        $data['datesend']  = $request->datesend;
        if ($request->hasFile('video')) {
            $file_ext=$request->video->getClientOriginalExtension() ;
            $file_name=time().'.'.$file_ext;
            $path='images';
            $request->video->move($path,$file_name);
            $data['video']=$file_name;
        }
            if ($request->hasFile('image')) {
                $file_ext=$request->image->getClientOriginalExtension() ;
                $file_name=time().'.'.$file_ext;
                $path='images';
                $request->image->move($path,$file_name);
                $data['image']=$file_name;
            }


           $notification= Notification::create($data);

           return response()->json([
               'status' => true,
               'message' => 'notification Created Successfully',
               'notification' => $notification,
           ]);

       }


       public function update(Request $request,$id){

           $notification= Notification::findOrFail($id);
           $data['name']  = $request->name;
           $data['number'] = $request->number;
           $data['description'] = $request->description;
           $data['datesend']  = $request->datesend;
           if ($request->hasFile('video')) {

            $oldvideo = $notification->video;
            $video = $request->file('image');
            $data['video'] = $this->videos($video, $oldvideo);
        }
        if ($request->hasFile('image')) {

            $oldimage = $notification->image;
            $image = $request->file('image');
            $data['image'] = $this->images($image, $oldimage);
        }

           $notification->update($request->all());
               return response()->json([
                   'status'=>true,
                   'data'=>$notification,
                   'message' => 'notification Updated Successfully',
               ]);
       }

       public function destroy($id)
       {
           $notification = Notification::findOrFail($id);
               $notification->delete();
           return response()->json([
               'status'=>true,
               'message' => 'Request Information deleted Successfully',
           ]);
           }
       }

