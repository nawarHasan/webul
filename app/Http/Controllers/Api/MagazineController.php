<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Team;
use App\Models\Magazine;

class MagazineController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(){

         $magazines = Magazine::all();
         return  response([
            'magazines'=>$magazines
        ],200);
        }

        public function store(Request $request){


            $data['name'] = $request->name;
            $data['author'] = $request->author;
            $data['title'] = $request->title;
            $data['datesend'] = $request->datesend;
            $data['description'] = $request->description;
            if ($request->hasFile('image')) {
                $file_ext=$request->image->getClientOriginalExtension() ;
                $file_name=time().'.'.$file_ext;
                $path='images';
                $request->image->move($path,$file_name);
                $data['image']=$file_name;
            }

            $magazine= magazine::create($data);

            return response()->json([
                'status' => true,
                'message' => 'Team Created Successfully',
                'magazine' => $magazine,
            ]);

        }


        public function update(Request $request,$id){

            $magazine= Magazine::findOrFail($id);
            $data['name'] = $request->name;
            $data['author'] = $request->author;
            $data['title'] = $request->title;
            $data['datesend'] = $request->datesend;
            $data['description'] = $request->description;

            if ($request->hasFile('image')) {

                $oldimage = $magazine->image;
                $image = $request->file('image');
                $data['image'] = $this->images($image, $oldimage);
            }
            $magazine->update($request->all());
                return response()->json([
                    'status'=>true,
                    'data'=>$magazine,
                    'message' => 'team Updated Successfully',
                ]);
        }

        public function destroy($id)
        {
            $magazine = Magazine::findOrFail($id);
                $magazine->delete();
            return response()->json([
                'status'=>true,
                'message' => 'Request Information deleted Successfully',
            ]);
            }
        }

