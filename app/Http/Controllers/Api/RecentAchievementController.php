<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\RecentAchievement;
use Illuminate\Http\Request;

class RecentAchievementController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(){


        $recent_achievements= RecentAchievement::all();
        return  response([
           'recent_achievements'=>$recent_achievements
       ],200);
       }

       public function store(Request $request){


           $data['name']  = $request->name;
           $data['description'] = $request->description;



           $recent_achievement= RecentAchievement::create($data);

           return response()->json([
               'status' => true,
               'message' => 'recent_achievement Created Successfully',
               'recent_achievement' => $recent_achievement,
           ]);

       }


       public function update(Request $request,$id){

           $recent_achievement= RecentAchievement::findOrFail($id);
           $data['name']  = $request->name;
           $data['description'] = $request->description;



           $recent_achievement->update($request->all());
               return response()->json([
                   'status'=>true,
                   'data'=>$recent_achievement,
                   'message' => 'recent_achievement Updated Successfully',
               ]);
       }

       public function destroy($id)
       {
           $recent_achievement = RecentAchievement::findOrFail($id);
               $recent_achievement->delete();
           return response()->json([
               'status'=>true,
               'message' => 'Request Information deleted Successfully',
           ]);
           }
       }
