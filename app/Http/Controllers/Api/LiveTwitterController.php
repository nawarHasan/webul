<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\LiveTwitter;
use Illuminate\Http\Request;

class LiveTwitterController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(){


        $livetwitters = LiveTwitter::all();
        return response([
           'livetwitters'=>$livetwitters
       ],200);
       }

       public function store(Request $request){

           $data['description'] = $request->description;
           $data['link'] = $request->link;

           $livetwitter= LiveTwitter::create($data);

           return response()->json([
               'status' => true,
               'message' => 'LiveTwitter Created Successfully',
               'livetwitters' => $livetwitter,
           ]);

       }


       public function update(Request $request,$id){

           $livetwitter= LiveTwitter::findOrFail($id);
           $data['description'] = $request->description;
           $data['link'] = $request->link;


           $livetwitter->update($request->all());
               return response()->json([
                   'status'=>true,
                   'data'=>$livetwitter,
                   'message' => 'livetwitter Updated Successfully',
               ]);
       }

       public function destroy($id)
       {
           $livetwitter = LiveTwitter::findOrFail($id);
               $livetwitter->delete();
           return response()->json([
               'status'=>true,
               'message' => 'Request Information deleted Successfully',
           ]);
           }
        }

