<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\LiveInstagram;
use Illuminate\Http\Request;

class LiveInstagramController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(){


        $liveinstagrams = LiveInstagram::all();
        return response([
           'liveinstagrams'=>$liveinstagrams
       ],200);
       }

       public function store(Request $request){

           $data['description'] = $request->description;
           $data['link'] = $request->link;

           $liveinstagram= LiveInstagram::create($data);

           return response()->json([
               'status' => true,
               'message' => 'LiveInstagram Created Successfully',
               'liveinstagram' => $liveinstagram,
           ]);

       }


       public function update(Request $request,$id){

           $liveinstagram= LiveInstagram::findOrFail($id);
           $data['description'] = $request->description;
           $data['link'] = $request->link;

           $liveinstagram->update($request->all());
               return response()->json([
                   'status'=>true,
                   'data'=>$liveinstagram,
                   'message' => 'liveinstagram Updated Successfully',
               ]);
       }

       public function destroy($id)
       {
           $liveinstagram = LiveInstagram::findOrFail($id);
               $liveinstagram->delete();
           return response()->json([
               'status'=>true,
               'message' => 'Request Information deleted Successfully',
           ]);
           }
        }
