<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\FollowUpPages;
use Illuminate\Http\Request;

class FollowUpPagesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(){


        $follow_up_pages = FollowUpPages::all();
        return  response([
           'follow_up_pages'=>$follow_up_pages
       ],200);
       }

       public function store(Request $request){


        $data['facebook']  = $request->facebook;
        $data['youtube'] = $request->youtube;
        $data['instagram'] = $request->instagram;
        $data['twitter'] = $request->twitter;
        $data['linkedin'] = $request->linkedin;

           $follow_up_page= FollowUpPages::create($data);

           return response()->json([
               'status' => true,
               'message' => 'follow_up_page Created Successfully',
               'follow_up_page' => $follow_up_page,
           ]);

       }


       public function update(Request $request,$id){

           $follow_up_page= FollowUpPages::findOrFail($id);
           $data['facebook']  = $request->facebook;
           $data['youtube'] = $request->youtube;
           $data['instagram'] = $request->instagram;
           $data['twitter'] = $request->twitter;
           $data['linkedin'] = $request->linkedin;


           $follow_up_page->update($request->all());
               return response()->json([
                   'status'=>true,
                   'data'=>$follow_up_page,
                   'message' => 'follow_up_page Updated Successfully',
               ]);
       }

       public function destroy($id)
       {
           $follow_up_page = FollowUpPages::findOrFail($id);
               $follow_up_page->delete();
           return response()->json([
               'status'=>true,
               'message' => 'Request Information deleted Successfully',
           ]);
           }
       }

