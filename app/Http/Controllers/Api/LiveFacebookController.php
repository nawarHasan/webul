<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\LiveFacebook;
use Illuminate\Http\Request;

class LiveFacebookController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(){


        $livefacebooks = LiveFacebook::all();
        return  response([
           'livefacebooks'=>$livefacebooks
       ],200);
       }

       public function store(Request $request){

           $data['description'] = $request->description;
           $data['link'] = $request->link;

           $live= LiveFacebook::create($data);

           return response()->json([
               'status' => true,
               'message' => 'live Created Successfully',
               'live' => $live,
           ]);

       }


       public function update(Request $request,$id){

           $livefacebook= LiveFacebook::findOrFail($id);
           $data['description'] = $request->description;
           $data['link'] = $request->link;


           $livefacebook->update($request->all());
               return response()->json([
                   'status'=>true,
                   'data'=>$livefacebook,
                   'message' => 'livefacebook Updated Successfully',
               ]);
       }

       public function destroy($id)
       {
           $livefacebook = LiveFacebook::findOrFail($id);
               $livefacebook->delete();
           return response()->json([
               'status'=>true,
               'message' => 'Request Information deleted Successfully',
           ]);
           }
        }
