<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Faq;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(){


         $faqs = Faq::all();
         return  response([
            'faqs'=>$faqs
        ],200);
        }

        public function store(Request $request){


            $data['answer']  = $request->answer;
            $data['question'] = $request->question;


            $faq= Faq::create($data);

            return response()->json([
                'status' => true,
                'message' => 'Faq Created Successfully',
                'faq' => $faq,
            ]);

        }


        public function update(Request $request,$id){

            $faq= Faq::findOrFail($id);
            $data['answer']  = $request->answer;
            $data['question'] = $request->question;


            $faq->update($request->all());
                return response()->json([
                    'status'=>true,
                    'data'=>$faq,
                    'message' => 'User Updated Successfully',
                ]);
        }

        public function destroy($id)
        {
            $faq = Faq::findOrFail($id);
                $faq->delete();
            return response()->json([
                'status'=>true,
                'message' => 'Request Information deleted Successfully',
            ]);
            }
        }
